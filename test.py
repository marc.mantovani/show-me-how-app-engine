import threading
from multiprocessing.pool import ThreadPool

import requests
from requests import Response

from constant import URL, THREADS_NUMBER, REQUESTS_NUMBER


def my_request(i: int) -> None:
    response: Response = requests.get(URL)
    print(
        f"Request {i} done by thread {threading.get_ident()} - response (HTTP {response.status_code}): {response.text} ")


if __name__ == '__main__':
    with ThreadPool(THREADS_NUMBER) as t:
        t.map(my_request, range(REQUESTS_NUMBER))
