import logging
import time
from typing import *

from flask import Flask
from google.cloud.logging import Client as GCPLoggingClient
from googletrans import Translator
from werkzeug.exceptions import HTTPException

from constant import *

client: GCPLoggingClient = GCPLoggingClient()
client.setup_logging()

app: Flask = Flask(__name__)  # App Engine will look for an app called `app` in `main.py`.

translator: Translator = Translator()


@app.route('/_ah/start')
def start() -> Tuple[str, int]:
    logging.info('Starting instance...')
    return 'OK', HTTP_200_OK


@app.route('/')
def index() -> Tuple[str, int]:
    logging.info('Entered on / endpoint')
    bg_color: str = 'PaleTurquoise'
    # bg_color: str = 'PaleGreen'       # TODO: uncomment this for blue/green deployment
    html: str = f'''
        <body style="background-color:{bg_color};">
            Hello world!
        </body>'''
    logging.info('Finished the process')
    return html, HTTP_200_OK


@app.route('/work')
def simulate_work() -> Tuple[str, int]:
    logging.info('Entered on /work endpoint')
    logging.info('Started working on the process')
    time.sleep(2)
    logging.info('Finished the process')
    return "OK", HTTP_200_OK


@app.route('/translate/<source>/<destination>/<text>')
def translate(source: str, destination: str, text: str) -> Tuple[str, int]:
    logging.info('Entered on /translate endpoint')
    translation: str = translator.translate(text,
                                            dest=destination,
                                            src=source).text
    logging.info(f'Translation from {source!r} to {destination!r} of the text {text!r}: {translation!r}')
    return translation, HTTP_200_OK


@app.route('/buggy')
def buggy() -> Tuple[str, int]:
    logging.info('Entered on /translate endpoint')
    return "OK", HTTP_200_OK
    # raise Exception('Buggy test')  # TODO: uncomment this for blue/green deployment


@app.errorhandler(HTTPException)
def handle_bad_request(e: HTTPException) -> Tuple[str, int]:
    logging.error(e)
    return f'Error - status code: {e.code}', e.code


if __name__ == '__main__':
    # This is used when running locally only. When deploying to Google App Engine, a webserver process such as Gunicorn will serve the app.
    logging.debug('Running locally')
    app.run(host='127.0.0.1',
            port=8080,
            debug=True)
